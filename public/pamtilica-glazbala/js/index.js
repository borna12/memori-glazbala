// Memory Game
// © 2014 Nate Wiley
// License -- MIT
// best in full screen, works on phones/tablets (min height for game is 500px..) enjoy ;)
// Follow me on Codepen

var razina = 1;
var broj_karata = 3;



$(document).ready(function() {
    promijeni_jezik()
});


function promijeni_jezik() {
let searchParams = new URLSearchParams(window.location.search)
    searchParams.has('j') // true
    let param = searchParams.get('j')

    if (param == "hr") {
        localStorage.setItem('jezik', 'hr')
    } else if (param == "eng") {
        localStorage.setItem('jezik', 'eng')
    } else if (localStorage.getItem("jezik") == null) {
        localStorage.setItem('jezik', 'hr')
    }


    if (localStorage.getItem("jezik") == null) {
        localStorage.setItem('jezik', 'hr')
    }
    if (localStorage.getItem("jezik") == "hr") {

        $("#hr").css({
            "opacity": "1"
        })
        $("#eng").css({
            "opacity": "0.5"
        })
        $(".winner").text("Odaberi broj parova: ")
        $("#link_nazad").text("ODABERI DRUGU IGRU")

    } else {
        localStorage.setItem('jezik', 'eng')
        $("#eng").css({
            "opacity": "1"
        })
        $("#hr").css({
            "opacity": "0.5"
        })
        $(".winner").text("Select number of pairs: ")
        $("#link_nazad").text("CHOOSE ANOTHER GAME")
    }
}


zastave = '<div class="centar"><a href="?j=hr" id="hr" > <img src="../zastavice/croatia.png" class="zastavice" ></a><a href="?j=eng" id="eng" > <img src="../zastavice/united-states.png" class="zastavice" ></a></div>'

$(".modal").html(zastave + "<h2 class='winner'>Odaberi broj parova:</h2><button id='prva'>4</button> <button id='druga'>8</button><button id='treca'>12</button>");

$("#hr").click(function() {
    localStorage.setItem('jezik', 'hr');
    promijeni_jezik()
})

$("#eng").click(function() {
    localStorage.setItem('jezik', 'eng');
    promijeni_jezik()
})

$("#prva").click(function() {
    razina = "1";
    igra()
})
$("#druga").click(function() {
    razina = "2";
    igra()
})
$("#treca").click(function() {
    razina = "3";
    igra()
})


function igra() {
    if (razina == 1) {
        broj_karata = 4;

    } else if (razina == 2) {
        broj_karata = 8;
    } else {
        broj_karata = 12
    }
    $("footer").fadeIn(1000);
    $(".modal").fadeOut(1000);
    $(".modal-overlay").delay(1000).slideUp(1000);
    $(".game").show("slow");
    //localStorage.clear();

    var br = 1;
    var sec = 0;
    var pokusaj = 0;
    var vrijeme = 1;

    var najbolje_vrijeme;
    var najmanji_broj_pokusaja;
    var karte;

    function pad(val) {
        return val > 9 ? val : "0" + val;
    }
    setInterval(function() {
        if (vrijeme == 1) {
            $("#seconds").html(pad(++sec % 60));
            $("#minutes").html(pad(parseInt(sec / 60, 10)));
        }
    }, 1000);
    var Memory = {

        init: function(cards) {
            this.$game = $(".game");
            this.$modal = $(".modal");
            this.$overlay = $(".modal-overlay");
            this.$restartButton = $(".restart");
            this.cardsArray = $.merge(cards, cards);

            this.shuffleCards(this.cardsArray);

            this.setup();
        },

        shuffleCards: function(cardsArray) {
            this.$cards = $(this.shuffle(this.cardsArray));
        },

        setup: function() {
            this.html = this.buildHTML();
            this.$game.html(this.html);
            this.$memoryCards = $(".card");
            this.binding();
            this.paused = false;
            this.guess = null;
            this.$cards = $(this.shuffle(this.cardsArray));

        },

        binding: function() {
            this.$memoryCards.on("click", this.cardClicked);
            this.$restartButton.on("click", $.proxy(this.reset, this));
        },
        // kinda messy but hey
        cardClicked: function() {

            var _ = Memory;
            var $card = $(this);
            if (!_.paused && !$card.find(".inside").hasClass("matched") && !$card.find(".inside").hasClass("picked")) {

                $card.find(".inside").addClass("picked");
                if (!_.guess) {
                    _.guess = $(this).attr("data-id");
                    $(this).find('p').toggle();
                } else if (_.guess == $(this).attr("data-id") && !$(this).hasClass("picked")) {
                    $(".picked").addClass("matched");
                    _.guess = null;
                    $(".matched").find('p').remove();
                    pokusaj++;

                    if (localStorage.getItem("jezik") == "hr") {
                        switch ($(this).attr('data-id')) {
                            case "1":
                                vrijeme = 0;
                                swal({
                                    title: 'Truba - bučina<br>Valpovo, sredina 19. st.',
                                    html: '<img src="slike/Bucпina-truba.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;"><em>Bučina</em> je glazbalo koje se izrađivalo od suhe tikve kojoj se odrezao vrh s jedne strane i polovica okruglog dijela s druge strane, izvadio se unutarnji dio, očistio od koštica i stavio sušiti u dimnjak kuće. Unutarnji se dio potom vrtio nad slabim plamenom kako bi se dodatno ispržile i one preostale niti. U uski se otvor najčešće utaknuo pisak i u njega se puhalo.</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'dalje',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/

                                }, function(isConfirm) {

                                });

                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "2":
                                vrijeme = 0;
                                swal({
                                    title: 'Diple<br>Zelovo, Sinj, sredina 20. st.',
                                    html: '<img src="slike/Diple.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;">Diple su dvocijevne sviraljke s rupicama i jednostrukim jezičkom tipa klarineta uglavnom izrađene od javorova drveta. Nekoć rasprostranjene cijelom Dalmacijom danas su se održale u dalmatinskom zaleđu, sjevernoj Dalmaciji i nekim otocima, primjerice Korčuli.</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'dalje',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/

                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "3":
                                vrijeme = 0;
                                swal({
                                    title: 'Panova svirala – trstenica<br>Trški vrh, 1913. ',
                                    html: '<img src="slike/Trstenice.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;">Trstenice, orgljice (panove svirale) svirale su se u Hrvatskom zagorju i sjevernije prema Međimurju prelazeći hrvatsku granicu prema Mariboru i šire. Radi se o glazbalu čije su cijevi u koje se upuhuje zrak izrađene od trstike, povezane u nizu između dviju daščica. Andske folklorne glazbene skupine kao putujući svirači proširile su andsku inačicu panovih svirala koja je danas u Hrvatskoj puno popularnija i poznatija od one iz Hrvatskog zagorja. Panove svirale iz Hrvatskog zagorja danas više nisu u uporabi a nekad su se koristile kao solističko glazbalo koje su svirali uglavnom pastiri.</p>',
                                    confirmButtonText: 'dalje',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/


                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "4":
                                vrijeme = 0;
                                swal({
                                    title: 'Frula <br>Rinkovec, Bednja, 1923.',
                                    html: '<img src="slike/Jedinka.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;">Jednocijevne sviraljke tipa flaute s nizom rupica za prebiranje, ovisno o lokalitetu, imaju razne nazive: <em>žveglica, jedinka, duduk, svirala, ćurominka, slavić, fistula, ćurlik, kavela, strančica</em>. Ova su se glazbala koristila kao solistička i signalna glazbala, a izrađivana su od drveta. Često su ih izrađivali pastiri za vlastitu zabavu. Ovakav tip glazbala rasprostranjen je među svim južnim Slavenima. </p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'dalje',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/


                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "5":
                                vrijeme = 0;
                                swal({
                                    title: 'Dude<br>nepoznato mjesto,  1. pol. 19. st.',
                                    html: '<img src="slike/Dude.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;">Dude su aerofono glazbalo koje se sastoji od mješine, puhalice, <em>karaba</em> i berde. Dude su se koristile baš kao i gajde, tijekom svadbe, te kod svih ostalih svjetovnih svečanosti. Tijekom crkvenih svečanosti su se gajde i dude ponekad svirale u crkvi. Kasnije su ih po gradovima i većim mjestima zamijenile orgulje.</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'dalje',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/


                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();
                                });
                                break;
                            case "6":
                                vrijeme = 0;
                                swal({
                                    title: 'Sopila - malo sopjelo<br>Rijeka, 1875. g.',
                                    html: '<img src="slike/Sopile.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;">Sopile i roženice karakteristične su za Istru, Hrvatsko primorje i kvarnerske otoke. One se obično sviraju u paru kao mala i vela sopila i najčešće su pratnja plesu. Na Barbanštini, u srednjoj i južnoj Istri, obično ih nazivaju <em>roženice</em>, u okolici Žminja <em>supiele</em>, na Labinštini, u Rudanima i još nekim selima <em>sopele</em>, a u Hrvatskom primorju i na otocima <em>sopele</em> ili <em>sopile</em>.</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'dalje',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/


                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "7":
                                vrijeme = 0;
                                swal({
                                    title: 'Grmavica - germjavica <br>Vrbnik, Krk, 2. des. 20. st. ',
                                    html: '<img src="slike/Grmavica.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;">Grmavica se sastoji iz više komada drvenih daščica međusobno privezanih špagom. Brzim okretanjem proizvodi se zvuk sličan grmljavini prema čemu je grmavica i dobila naziv. Za ovo je glazbalo zabilježen i naziv <em>zujača</em>  na otoku Krku.</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'dalje',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/


                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "8":
                                vrijeme = 0;
                                swal({
                                    title: 'Gajde<br>Kistanje, 1928.',
                                    html: '<img src="slike/Gajde.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;">Gajde su aerofono glazbalo koje se od nekoliko dijelova: <em>mješine</em> (dio koji služi kao spremište zraka), <em>puhalice</em> (dio u koji se upuhuje zrak i prolazi do mješine), <em>prebiralice</em> (koja se još naziva i <em>gajdunica</em>). </p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'dalje',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/


                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "9":
                                vrijeme = 0;
                                swal({
                                    title: 'Rog noćnih stražara<br>Aljmaš, 2. pol. 19. st.',
                                    html: '<img src="slike/rog-nocnih-strazara.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;">Životinjski rogovi kao što su <em>kravarski rog, rog noćnih stražara i kozarski rog</em> izrađeni su od roga vola, bivola ili koze. Rog bi se odstranio sa životinje, dobro očistio s unutarnje i vanjske strane te dodatno ostrugao suhom travom kako bi bio što glađi. <em>Rog noćnih stražara</em>, za razliku od druga dva, ima još dodatni pisak od trske koji se umeće u tanji otvor te pričvršćuje lanom i lijepi voskom.</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'dalje',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/


                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "10":
                                vrijeme = 0;
                                swal({
                                    title: 'Lira - lirica <br>Blato, Korčula - 1. pol. 20. st. ',
                                    html: '<img src="slike/Lira.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;">Lira ili lijerica solističko je glazbalo namijenjeno ponajprije glazbenoj pratnji plesa, primjerice <em>linđa</em> ili <em>poskočice</em>.  Lijerica je i danas u živoj uporabi u Dubrovačkom primorju, Konavlima i na poluotoku Pelješcu te na otocima Mljetu i Lastovu.</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'dalje',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/


                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "11":
                                vrijeme = 0;
                                swal({
                                    title: 'Bubanj <br>Ivankovo, 1. pol. 20. st.',
                                    html: '<img src="slike/Buba.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;">Bubanj iz Ivankova izrađen je iz jednog komada drveta unutar kojeg je kao opna navučena janjeća koža, a svira se uz pomoć dvaju batića kojima se udara po opni. Ovo se glazbalo koristilo kao pratnja <em>dvojnicama</em> ili <em>tamburama dvožicama</em> na većim slavljima i u svatovima.</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'dalje',
                                    allowOutsideClick: false

                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "12":
                                vrijeme = 0;
                                swal({
                                    title: 'Tambura – bugarija<br>Rijeka, oko 1900.',
                                    html: '<img src="slike/Tambura.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;">Tambura je trzalačko kordofono glazbalo na kojem se zvuk proizvodi trzanjem trzalicom po žicama. Sastoji se od tri osnovna dijela: trupa, vrata s hvataljkom i glave. Trup (koji se još naziva i <em>korpus, zvekalo, tijelo</em>) može biti izrađen iz izdubljenog komada drveta klena, šljive, javora, duda, oraha, jasena i slično.</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'dalje',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/

                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;


                        }
                    }
                    /*eng opis*/
                    else {
                        switch ($(this).attr('data-id')) {
                            case "1":
                                vrijeme = 0;
                                swal({
                                    title: 'The trumpet - bučina<br>Valpovo, middle of the 19th century',
                                    html: '<img src="slike/Bucпina-truba.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;"><em>The bučina</em> is an instrument made from dried gourd by cutting off its top on one side and a half of its round body on the other side, scooping it out, removing its seeds and leaving it to dry in the chimney. Its inside was then spun above faint flame for the remaining fibres to be burnt. A reed was generally inserted in the narrow opening into which air was blown.</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'next',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/
                                }, function(isConfirm) {

                                });

                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "2":
                                vrijeme = 0;
                                swal({
                                    title: '<strong>The diple<br>Zelovo, Sinj, the middle of 20th century</strong>',
                                    html: '<img src="slike/Diple.jpg" class="ikone"/><p style="text-align:justify;">Diple consist of two pipes with finger holes and a single reed of the clarinet type, mostly made of maple wood. They were once widespread in the whole of Dalmatia and today they are kept in the Dalmatian hinterland, northern Dalmatia and some of the islands such as Korčula.</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'next',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/
                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "3":
                                vrijeme = 0;
                                swal({
                                    title: 'Pan flute - trstenica<br>Trški vrh, 1913',
                                    html: '<img src="slike/Trstenice.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;"><em>Trstenice, orgljice (panove svirale – the pan flute)</em> were played in the area from Hrvatsko Zagorje further to the north to Međimurje and, crossing the Croatian border, to Maribor and beyond. It is a musical instrument with pipes for blowing air which are made of equally thick pieces of cane attached to each other in a series between two slats. Andean folk music ensembles that travel round the world have been spreading their own variant of <em>the pan flute</em>, which is today even more popular in Croatia than the indigenous variant from Hrvatsko Zagorje. The <em>pan flute</em> from Hrvatsko Zagorje was once used as a solo instrument mostly played by herdsmen but today it has fallen out of use.</p>',
                                    confirmButtonText: 'next',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/
                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "4":
                                vrijeme = 0;
                                swal({
                                    title: 'Fife <br> Rinkovec, Bednja, 1923',
                                    html: '<img src="slike/Jedinka.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;">Single-reed instruments such as the flute, with a series of finger holes, have different names depending on their place of origin: <em>žveglica, jedinka, duduk, svirala, ćurominka, slavić, fistula, ćurlik, kavela, strančica</em>. Those were solo and signal instruments made of wood. They were often built by shepherds for their own entertainment and they were widespread among all South Slavs.</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'next',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/
                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "5":
                                vrijeme = 0;
                                swal({
                                    title: 'Dude<br> unknown place,  1st half of the 19th century',
                                    html: '<img src="slike/Dude.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;">The dude are an aerophone consisting of <em>bellows, chanter</em> and <em>berde</em>. Just like the gajde, the dude were used during wedding ceremonies and other civil ceremonies. During church ceremonies both <em>the gajde</em> and <em>the dude</em> were played in the church. They were subsequently replaced by organ in cities and towns.</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'next',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/
                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();
                                });
                                break;
                            case "6":
                                vrijeme = 0;
                                swal({
                                    title: 'The small sopila<br> Rijeka, 1875',
                                    html: '<img src="slike/Sopile.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;"><em>Sopile</em> and <em>roženice</em> are typical of Istria, the Croatian Littorial and the Kvarner islands. They are usually played in pair – like the <em>small and the large sopila</em> and are most often an accompaniment for dance. They are also called <em>roženice</em> – in Barbanština (Barban area), central and southern Istria; <em>supiele</em> – around Žminj; </em>sopele</em> in Labinština (Labin area), Rudani and some other Istrian villages and <em>sopele</em> or <em>sopile</em> in the Croatian Littorial and the islands.</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'next',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/
                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "7":
                                vrijeme = 0;
                                swal({
                                    title: 'The thunderer<br>Vrbnik, island of Krk, 2nd decade of the 20th century',
                                    html: '<img src="slike/Grmavica.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;"><em>The thunderer</em> consists of several wooden plates tied together with a rope. When it is spun fast it produces sound similar to that of thunder, which is where the instrument gets its name from. This instrument was called <em>zujača</em> (buzzer) on the island of Krk.</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'next',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/
                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "8":
                                vrijeme = 0;
                                swal({
                                    title: 'The bagpipe<br>Kistanje, 1928 ',
                                    html: '<img src="slike/Gajde.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;"><em>The gajde</em> (bagpipe) is an aerophone musical instrument which consists of several parts: a <em>bag</em> (air reservoir), a <em>blowpipe</em> (into which air going to the bag is blown), a <em>chanter</em> (also called gajdunica).</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'next',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/
                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "9":
                                vrijeme = 0;
                                swal({
                                    title: 'The night watchmen’s horn<br> Aljmaš, 2nd half of the 20th century',
                                    html: '<img src="slike/rog-nocnih-strazara.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;"><em>Animal horns</em> such as the <em>cowherd’s horn</em>, the <em>night watchmen’s horn</em>, and the <em>goatherd’s horn</em> were made from the horn of an ox, a buffalo or a goat. The horn was removed from the animal, properly cleaned, both inside and outside, and further scraped with dry grass for smoothening. Unlike the other two, the <em>night watchmen’s horn</em> has an additional cane reed that is inserted into a slot, secured with flax and sealed with wax. </p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'next',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/
                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "10":
                                vrijeme = 0;
                                swal({
                                    title: 'The lyre<br>Blato, Korčula, 1st half of the 20th century',
                                    html: '<img src="slike/Lira.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;">The lyre (also called <em>lijerica</em>) is a solo musical instrument intended primarily for a musical accompaniment for dance – such as <em>linđo</em> (a folk dance of the region of Dubrovnik) or the so-called <em>poskočice</em>. The <em>lijerica</em> is still very much used in the Dubrovnik Littorial, Konavle and the Pelješac penisula as well as on the islands of Mljet and Lastovo.</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'next',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/
                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "11":
                                vrijeme = 0;
                                swal({
                                    title: 'The drum<br>Ivankovo, the 1st half of the 20th century',
                                    html: '<img src="slike/Buba.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;">The drum is crafted as one single piece - a wooden hoop and tanned lambskin stretched over it. The sound is produced when the membrane is struck by two beaters. This instrument accompanied <em>the flute</em> or the <em>two-stringed tambura</em> at weddings or other festive occasions.</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'next',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/
                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                            case "12":
                                vrijeme = 0;
                                swal({
                                    title: 'The tamboura (called bugarija)<br> Rijeka, around 1900',
                                    html: '<img src="slike/Tambura.jpg" class="ikone"/>' +
                                        '<p style="text-align:justify;">The tamboura is a plucked chordophone musical instrument and its sound is produced by plucking strings with a plectrum, It has three main parts: the body, the neck with the fingerboard and the head. The body (also called <em>korpus</em> or <em>zvekalo</em>) can be made from a hollowed out piece of common maple wood, plum wood, mulberry, walnut, ash wood etc.</p>',
                                    showCloseButton: true,
                                    confirmButtonText: 'next',
                                    /*allowOutsideClick: false,*/
                                    /*allowEscapeKey: false*/
                                }, function(isConfirm) {

                                });
                                $('.swal2-confirm, .swal2-close').click(function() {
                                    vrijeme = 1;
                                    $.stopSound();

                                });
                                break;
                        }
                    }
                } else {
                    pokusaj++;
                    $(this).find('p').toggle();
                    _.guess = null;
                    _.paused = true;
                    setTimeout(function() {
                        $(".picked").removeClass("picked");
                        Memory.paused = false;
                        $(".brojevi").show();
                    }, 1200);
                }
                if ($(".matched").length == $(".card").length) {
                    _.win();
                }
            }
        },

        win: function() {
            this.paused = true;
            setTimeout(function() {
                Memory.showModal();
                Memory.$game.fadeOut();

            }, 1000);
        },

        showModal: function() {
            var minute = Math.floor(sec / 60);
            var sekunde = sec - minute * 60;
            this.$overlay.show();
            this.$modal.fadeIn("slow");

            if (razina == 1) {
                var najvrijeme = localStorage.getItem('najvrijeme');
                if (najvrijeme === undefined || najvrijeme === null) {
                    najvrijeme = sec;
                    localStorage.setItem('najvrijeme', sec);
                }
                // If the user has more points than the currently stored high score then
                if (sec < najvrijeme) {
                    // Set the high score to the users' current points
                    najvrijeme = sec;
                    // Store the high score
                    localStorage.setItem('najvrijeme', sec);
                }


                var najpokusaji = localStorage.getItem('najpokusaji');

                if (najpokusaji === undefined || najpokusaji === null) {
                    najpokusaji = pokusaj;
                    localStorage.setItem('najpokusaji', pokusaj);
                }

                // If the user has more points than the currently stored high score then
                if (pokusaj < najpokusaji) {
                    // Set the high score to the users' current points
                    najpokusaji = pokusaj;
                    // Store the high score
                    localStorage.setItem('najpokusaji', najpokusaji);
                }
            } else if (razina == 2) {
                var najvrijeme = localStorage.getItem('najvrijeme2');
                if (najvrijeme === undefined || najvrijeme === null) {
                    najvrijeme = sec;
                    localStorage.setItem('najvrijeme2', sec);
                }
                // If the user has more points than the currently stored high score then
                if (sec < najvrijeme) {
                    // Set the high score to the users' current points
                    najvrijeme = sec;
                    // Store the high score
                    localStorage.setItem('najvrijeme2', sec);
                }

                var najpokusaji = localStorage.getItem('najpokusaji2');

                if (najpokusaji === undefined || najpokusaji === null) {
                    najpokusaji = pokusaj;
                    localStorage.setItem('najpokusaji2', pokusaj);
                }

                // If the user has more points than the currently stored high score then
                if (pokusaj < najpokusaji) {
                    // Set the high score to the users' current points
                    najpokusaji = pokusaj;
                    // Store the high score
                    localStorage.setItem('najpokusaji2', pokusaj);
                }
            } else {
                var najvrijeme = localStorage.getItem('najvrijeme3');
                if (najvrijeme === undefined || najvrijeme === null) {
                    najvrijeme = sec;
                    localStorage.setItem('najvrijeme3', sec);
                }
                // If the user has more points than the currently stored high score then
                if (sec < najvrijeme) {
                    // Set the high score to the users' current points
                    najvrijeme = sec;
                    // Store the high score
                    localStorage.setItem('najvrijeme3', sec);
                }


                var najpokusaji = localStorage.getItem('najpokusaji3');

                if (najpokusaji === undefined || najpokusaji === null) {
                    najpokusaji = pokusaj;
                    localStorage.setItem('najpokusaji3', pokusaj);
                }

                // If the user has more points than the currently stored high score then
                if (pokusaj < najpokusaji) {
                    // Set the high score to the users' current points
                    najpokusaji = pokusaj;
                    // Store the high score
                    localStorage.setItem('najpokusaji3', pokusaj);
                }
            }

            // Return the high score

            var naj_minute = Math.floor(najvrijeme / 60);
            var naj_sekunde = najvrijeme - naj_minute * 60;
            $(".modal").show();
            $(".modal-overlay").show();
            $(".winner").hide();


            if (localStorage.getItem("jezik") == "hr") {
                $(".modal").html("<div class='winner'>Bravo!</div><div class='time'><br>broj pokušaja: " + pokusaj + "</br>najmanji broj pokušaja u razini: " + najpokusaji + "<br></br></br>vrijeme igre: " + minute + ":" + sekunde + "</br>najbolje vrijeme: " + naj_minute + ":" + naj_sekunde + "<p><br><br><a id='reset' onclick='window.location.reload()'>nova igra</a></p></div>")
            } else {
                $(".modal").html("<div class='winner'>Good job!</div><div class='time'><br>number of tries: " + pokusaj + "</br>the least number of tries per level: " + najpokusaji + "<br></br></br>time: " + minute + ":" + sekunde + "</br>best time per level: " + naj_minute + ":" + naj_sekunde + "<p><br><br><a id='reset' onclick='window.location.reload()'>new game</a></p></div>")
            }

        },

        hideModal: function() {
            this.$overlay.hide();
            this.$modal.hide();
        },

        reset: function() {
            this.hideModal();
            this.shuffleCards(this.cardsArray);
            this.setup();
            this.$game.show("slow");
            pokusaj = 0;
            sec = 0;
            br = 1;
        },

        // Fisher--Yates Algorithm -- http://bost.ocks.org/mike/shuffle/
        shuffle: function(array) {
            var counter = array.length,
                temp, index;
            // While there are elements in the array
            while (counter > 0) {
                // Pick a random index
                index = Math.floor(Math.random() * counter);
                // Decrease counter by 1
                counter--;
                // And swap the last element with it
                temp = array[counter];
                array[counter] = array[index];
                array[index] = temp;
            }
            return array;
        },

        buildHTML: function() {
            var frag = '';
            br = 1;
            this.$cards.each(function(k, v) {
                frag += '<div class="card" data-id="' + v.id + '"><div class="inside">\
      <div class="front"><img src="' + v.img + '"\
      alt="' + v.name + '" /></div>\
      <div class="back"><p class="brojevi">' + br + '</p></div></div>\
      </div>';
                if (br < cards.length) {
                    br++;
                };
            });
            return frag;
        }
    };

    var cards = [{
        name: "Truba - bučina Valpovo, sredina 19. st.",
        img: "slike/Bucпina-truba.jpg",
        id: 1,
    }, {
        name: "Diple Zelovo, Sinj, sredina 20. st.",
        img: "slike/Diple.jpg",
        id: 2
    }, {
        name: "Panova svirala – trstenica Trški vrh, 1913. ",
        img: "slike/Trstenice.jpg",
        id: 3
    }, {
        name: "Frula Rinkovec, Bednja, 1923. ",
        img: "slike/Jedinka.jpg",
        id: 4
    }, {
        name: "Dude nepoznato mjesto,  1. pol. 19. st.",
        img: "slike/Dude.jpg",
        id: 5
    }, {
        name: "Sopila - malo sopjelo Rijeka, 1875. g.",
        img: "slike/Sopile.jpg",
        id: 6
    }, {
        name: "Grmavica – germjavica Vrbnik, Krk, 2. des. 20. st. ",
        img: "slike/Grmavica.jpg",
        id: 7
    }, {
        name: "Gajde Kistanje, 1928.",
        img: "slike/Gajde.jpg",
        id: 8
    }, {
        name: "Rog noćnih stražara Aljmaš, 2. pol. 19. st.",
        img: "slike/rog-nocnih-strazara.jpg",
        id: 9
    }, {
        name: "Lira - lirica Blato, Korčula - 1. pol. 20. st. ",
        img: "slike/Lira.jpg",
        id: 10
    }, {
        name: "Bubanj Ivankovo, 1. pol. 20. st.",
        img: "slike/Buba.jpg",
        id: 11
    }, {
        name: "Tambura bugarija Rijeka, oko 1900.",
        img: "slike/Tambura.jpg",
        id: 12
    }];

    function shuffle(array) {
        var currentIndex = array.length,
            temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    cards = shuffle(cards);

    cards = cards.slice(0, broj_karata);

    var brojKarata = cards.length;
    Memory.init(cards);

    if (razina == 1) {
        $(".card").css({
            "width": "25%",
            "height": "50%"
        })
    } else if (razina == 2) {
        $(".card").css({
            "width": "25%",
            "height": "25%"
        })
    } else if (razina == 3) {
        $(".card").css({
            "width": "16.66666%",
            "height": "25%"
        })
    }
}