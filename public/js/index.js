$("#hr").click(function() {
    localStorage.setItem('jezik', 'hr');
    promijeni_jezik()
})

$("#eng").click(function() {
    localStorage.setItem('jezik', 'eng');
    promijeni_jezik()
})


$("#prepoznaj").click(function() {
    window.open("prepoznaj-glazbalo", "_self")
})
$("#pamtilica").click(function() {
    window.open("pamtilica-glazbala", "_self")
})
$("#slagalice").click(function() {
    window.open("slagalice-glazbala", "_self")
})


$(document).ready(function() {
    promijeni_jezik()
    
    $("#godina").text(new Date().getFullYear())
});

function promijeni_jezik() {

    let searchParams = new URLSearchParams(window.location.search)
    searchParams.has('j') // true
    let param = searchParams.get('j')

    if (param == "hr") {
        localStorage.setItem('jezik', 'hr')
    } else if (param == "eng") {
        localStorage.setItem('jezik', 'eng')
    }

    if (localStorage.getItem("jezik") == null) {
        localStorage.setItem('jezik', 'hr')
    }
    if (localStorage.getItem("jezik") == "hr") {

        $("#hr").css({
            "opacity": "1"
        })
        $("#eng").css({
            "opacity": "0.5"
        })
        
        $("#naslov").text("Edukativne igre o glazbalima ♫")
        $( ".title:eq(0)" ).text("PAMTILICA")
        $( ".title:eq(1)" ).text("SLAGALICE")
        $( ".title:eq(2)" ).text("PREPOZNAJ GLAZBALA")
        
        $( ".subtitle:eq(0)" ).text("povežite glazbala te doznajte informacije o njima")
        $( ".subtitle:eq(1)" ).text("izaberite sliku glazbala za slaganje")
        $( ".subtitle:eq(2)" ).text("na temelju zvuka odaberite sliku odgovarajućeg glazbala")
    

    } else {
        localStorage.setItem('jezik', 'eng')
        $("#eng").css({
            "opacity": "1"
        })
        $("#hr").css({
            "opacity": "0.5"
        })
            $("#naslov").text("Educational music games ♫")
            $( ".title:eq(0)" ).text("MEMORY")
            $( ".title:eq(1)" ).text("PUZZLES")
            $( ".title:eq(2)" ).text("RECOGNIZE THE MUSIC INSTRUMENT")
            
        $( ".subtitle:eq(0)" ).text("match the pairs and learn more about the instruments")
        $( ".subtitle:eq(1)" ).text("choose a picture for puzzle solving")
        $( ".subtitle:eq(2)" ).text("based on a sound select the right instrument")
    }
}