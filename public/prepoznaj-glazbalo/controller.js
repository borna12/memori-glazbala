$(document).ready(function() {

    var questionNumber = 0;
    var questionBank = new Array();
    var stage = "#game1";
    var stage2 = new Object;
    var questionLock = false;
    var numberOfQuestions;
    var score = 0;
    var set = 0;
    var razina = 0;
    var moze = 0;
    var tocno, pogresno, upozorenje, kraj, gumb, toc_odg, nazad;

    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the <span> element that closes the modal
    var puhacki = document.getElementById("puhacki");
    var zicni = document.getElementById("zicni");
    var udaracki = document.getElementById("udaracki");


    $("#hr").click(function() {
        localStorage.setItem('jezik', 'hr');
        promijeni_jezik()
    })

    $("#eng").click(function() {
        localStorage.setItem('jezik', 'eng');
        promijeni_jezik()
    })

    function promijeni_jezik() {
        if (localStorage.getItem("jezik") == null) {
            localStorage.setItem('jezik', 'hr')
        }
        if (localStorage.getItem("jezik") == "hr") {

            $("#hr").css({
                "opacity": "1"
            })
            $("#eng").css({
                "opacity": "0.5"
            })

            $("#laka").text("lagano")
            $("#srednja").text("srednje")
            $("#teska").text("teško")
            $("#naslov").text("Prepoznaj glazbeni instrument")
            $("#tezo").text("odaberi težinu igre")
            $("#uputa").text("Pritinsite ikonu zvučnika, poslušajte zapis te pogodite o kojem se glazbalu radi.")
            tocno = "TOČNO"
            pogresno = "POGREŠNO"
            upozorenje = "MORATE PRVO POSLUŠATI ZVUK!"
            gumb = "nastavi igru"
            kraj = "provjeri rezultat"
            toc_odg = "Točan odgovor je: "
            nazad = "odaberi drugu igru"
            $(".nazad").text(nazad)
        } else {
            localStorage.setItem('jezik', 'eng')
            $("#eng").css({
                "opacity": "1"
            })
            $("#hr").css({
                "opacity": "0.5"
            })
            $("#laka").text("easy")
            $("#srednja").text("medium")
            $("#teska").text("hard")
            $("#naslov").text("Recognize the music instrument")
            $("#tezo").text("choose the difficulty of the game")
            $("#uputa").text("Press the speaker to hear the sound and then try to guess the instrument.")
            tocno = "CORRECT"
            pogresno = "WRONG"
            upozorenje = "PRESS THE SPEAKER FIRST!"
            gumb = "continue"
            kraj = "check the resoults"
            toc_odg = "The correct answer is: "
            nazad = "choose another game"
            $(".nazad").text(nazad)

        }
    }




    promijeni_jezik()

    // When the user clicks the button, open the modal 

    modal.style.display = "block";

    // When the user clicks on <span> (x), close the modal
    puhacki.onclick = function() {
        modal.style.display = "none";
        set = 1;
        odabir_pitanja();
    }

    zicni.onclick = function() {
        modal.style.display = "none";
        set = 3;
        odabir_pitanja();
    }

    udaracki.onclick = function() {
        modal.style.display = "none";
        set = 2;
        odabir_pitanja();
    }

    function izmjesaj(array) { //izmješaj pitanja
        var i = 0,
            j = 0,
            temp = null

        for (i = array.length - 1; i > 0; i -= 1) {
            j = Math.floor(Math.random() * (i + 1))
            temp = array[i]
            array[i] = array[j]
            array[j] = temp
        }
    }


    function odabir_pitanja() {
        $('#uputa, #broj').show();

        if (set == 1) {
            if (localStorage.getItem("jezik") == "hr") {
                datoteka = 'activity.json'
            } else {
                datoteka = 'activity-eng.json'
            }
            $.getJSON(datoteka, function(data) {

                for (i = 0; i < data.quizlist.length; i++) {
                    questionBank[i] = new Array;
                    questionBank[i][0] = data.quizlist[i].question;
                    questionBank[i][1] = data.quizlist[i].option1;
                    questionBank[i][2] = data.quizlist[i].option2;
                    questionBank[i][3] = data.quizlist[i].option3;
                    questionBank[i][4] = data.quizlist[i].audio;
                    questionBank[i][5] = data.quizlist[i].naslov;
                    questionBank[i][6] = data.quizlist[i].opis;
                }
                izmjesaj(questionBank);
                numberOfQuestions = questionBank.length;

                razina = 1;
                displayQuestion();
            })
        }
        if (set == 2) {
            if (localStorage.getItem("jezik") == "hr") {

                datoteka = 'activity_2.json'
            } else {
                datoteka = 'activity_2-eng.json'
            }
            $.getJSON(datoteka, function(data) {
                for (i = 0; i < data.quizlist.length; i++) {
                    questionBank[i] = new Array;
                    questionBank[i][0] = data.quizlist[i].question;
                    questionBank[i][1] = data.quizlist[i].option1;
                    questionBank[i][2] = data.quizlist[i].option2;
                    questionBank[i][3] = data.quizlist[i].option3;
                    questionBank[i][4] = data.quizlist[i].option4;
                    questionBank[i][5] = data.quizlist[i].option5;
                    questionBank[i][6] = data.quizlist[i].option6;
                    questionBank[i][7] = data.quizlist[i].audio;
                    questionBank[i][8] = data.quizlist[i].naslov;
                    questionBank[i][9] = data.quizlist[i].opis;
                }
                izmjesaj(questionBank);
                numberOfQuestions = questionBank.length;

                razina = 2;
                displayQuestion();
            })
        }
        if (set == 3) {

            if (localStorage.getItem("jezik") == "hr") {

                datoteka = 'activity_3.json'
            } else {
                datoteka = 'activity_3-eng.json'
            }


            $.getJSON(datoteka, function(data) {

                for (i = 0; i < data.quizlist.length; i++) {
                    questionBank[i] = new Array;
                    questionBank[i][0] = data.quizlist[i].question;
                    questionBank[i][1] = data.quizlist[i].option1;
                    questionBank[i][2] = data.quizlist[i].option2;
                    questionBank[i][3] = data.quizlist[i].option3;
                    questionBank[i][4] = data.quizlist[i].option4;
                    questionBank[i][5] = data.quizlist[i].option5;
                    questionBank[i][6] = data.quizlist[i].option6;
                    questionBank[i][7] = data.quizlist[i].option7;
                    questionBank[i][8] = data.quizlist[i].option8;
                    questionBank[i][9] = data.quizlist[i].option9;
                    questionBank[i][10] = data.quizlist[i].audio;
                    questionBank[i][11] = data.quizlist[i].naslov;
                    questionBank[i][12] = data.quizlist[i].opis;
                }
                izmjesaj(questionBank);
                numberOfQuestions = questionBank.length;
                razina = 3;
                displayQuestion();
            })
        }
    }

    //gtjson


    fillDB();



    function displayQuestion() {

        if (razina == 1) {
            var rnd = Math.random() * 3;
        } else if (razina == 2) {
            var rnd = Math.random() * 6;
        } else {
            var rnd = Math.random() * 9;
        }
        rnd = Math.ceil(rnd);
        var q1;
        var q2;
        var q3;
        var q4;
        var q5;
        var q6;
        var q7;
        var q8;
        var q9;

        if (razina == 1) {
            if (rnd == 1) {
                q1 = questionBank[questionNumber][1];
                q2 = questionBank[questionNumber][2];
                q3 = questionBank[questionNumber][3];
            }
            if (rnd == 2) {
                q2 = questionBank[questionNumber][1];
                q3 = questionBank[questionNumber][2];
                q1 = questionBank[questionNumber][3];

            }
            if (rnd == 3) {
                q3 = questionBank[questionNumber][1];
                q1 = questionBank[questionNumber][2];
                q2 = questionBank[questionNumber][3];
            }

            $(stage).append('<div  class="questionText"><div class="speaker" id="fix"></div><a href="#" class="dalje" style="display:none;">dalje &raquo;</a></div><div id="wrap"><div id="1" class="pix"><img src="zvuk/' + q1["naziv"] + '"><br><p class="tekst_uz_sliku">' + q1["tekst"] + '</p></div><div id="2" class="pix"><img src="zvuk/' + q2["naziv"] + '"><p class="tekst_uz_sliku">' + q2["tekst"] + '</p></div><div id="3" class="pix"><img src="zvuk/' + q3["naziv"] + '"><p class="tekst_uz_sliku">' + q3["tekst"] + '</p></div><footer><a href="../" class="nazad">' + nazad + '</a></footer></div><audio id = "player"> <source id="ogg" src="zvuk/' + questionBank[questionNumber][4] + '.ogg" type="audio/ogg"><source id="mp3" src="zvuk/' + questionBank[questionNumber][4] + '.mp3" type="audio/mp3"></audio>');
            $(".dalje").click(function() {
                changeQuestion();
            })
            $("footer").fadeIn(300)

            //puštanje zvuka
            var getaudio = $('#player')[0];
            /* Global variable for a timer. When the mouse is hovered over the speaker it will start playing after hovering for 1 second, if less than 1 second it won't play (incase you accidentally hover over the speaker) */
            var audiostatus = 'on';
            /* Global variable for the audio's status (off or on). It's a bit crude but it works for determining the status. */

            var zbuk = 0;
            $(document).on('click tap', '.speaker', function() {
                /* Touchend is necessary for mobile devices, click alone won't work */
                if (zbuk == 0) {
                    if (audiostatus == 'off') {
                        $(".speaker").css("background-position", "0 0");
                        getaudio.load();
                        getaudio.play();
                        zbuk = 1;
                        audiostatus = 'on';
                        return false;
                    } else if (audiostatus == 'on') {
                        $(".speaker").css("background-position", "0 0");
                        $('#player')[0].play()
                        moze = 1;
                        zbuk = 1;
                    }
                } else if (zbuk == 1) {
                    $('#player')[0].pause();
                    $(".speaker").css("background-position", "100% 0");

                    audiostatus = 'on';
                    zbuk = 0;
                }
            });

            $('#player').on('ended', function() {
                $(".speaker").css("background-position", "100% 0");
                /*When the audio has finished playing, remove the class speakerplay*/
                audiostatus = 'off';
                /*Set the status back to off*/
            });
        } else if (razina == 2) {
            if (rnd == 1) {
                q1 = questionBank[questionNumber][1];
                q2 = questionBank[questionNumber][2];
                q3 = questionBank[questionNumber][3];
                q4 = questionBank[questionNumber][4];
                q5 = questionBank[questionNumber][5];
                q6 = questionBank[questionNumber][6];
            }
            if (rnd == 2) {
                q2 = questionBank[questionNumber][1];
                q3 = questionBank[questionNumber][2];
                q1 = questionBank[questionNumber][3];
                q4 = questionBank[questionNumber][4];
                q6 = questionBank[questionNumber][5];
                q5 = questionBank[questionNumber][6];
            }
            if (rnd == 3) {
                q3 = questionBank[questionNumber][1];
                q1 = questionBank[questionNumber][2];
                q4 = questionBank[questionNumber][3];
                q2 = questionBank[questionNumber][4];
                q6 = questionBank[questionNumber][5];
                q5 = questionBank[questionNumber][6];
            }

            if (rnd == 4) {
                q4 = questionBank[questionNumber][1];
                q1 = questionBank[questionNumber][2];
                q2 = questionBank[questionNumber][3];
                q3 = questionBank[questionNumber][4];
                q5 = questionBank[questionNumber][5];
                q6 = questionBank[questionNumber][6];
            }
            if (rnd == 5) {
                q5 = questionBank[questionNumber][1];
                q1 = questionBank[questionNumber][2];
                q2 = questionBank[questionNumber][3];
                q3 = questionBank[questionNumber][4];
                q4 = questionBank[questionNumber][5];
                q6 = questionBank[questionNumber][6];
            }

            if (rnd == 6) {
                q6 = questionBank[questionNumber][1];
                q1 = questionBank[questionNumber][2];
                q2 = questionBank[questionNumber][3];
                q3 = questionBank[questionNumber][4];
                q5 = questionBank[questionNumber][5];
                q4 = questionBank[questionNumber][6];
            }



            $(stage).append(' <div class="questionText"> <div class="speaker" id="fix"></div><a href="#" class="dalje" style="display:none;">dalje &raquo;</a> </div><div id="wrap"> <div id="1" class="pix"><img src="zvuk/' + q1["naziv"] + '"> <br><p class="tekst_uz_sliku">' + q1["tekst"] + '</p></div><div id="2" class="pix"><img src="zvuk/' + q2["naziv"] + '"> <p class="tekst_uz_sliku">' + q2["tekst"] + '</p></div><div id="3" class="pix"><img src="zvuk/' + q3["naziv"] + '"> <p class="tekst_uz_sliku">' + q3["tekst"] + '</p></div><div id="4" class="pix"><img src="zvuk/' + q4["naziv"] + '"> <p class="tekst_uz_sliku">' + q4["tekst"] + '</p></div><div id="5" class="pix"><img src="zvuk/' + q5["naziv"] + '"> <p class="tekst_uz_sliku">' + q5["tekst"] + '</p></div><div id="6" class="pix"><img src="zvuk/' + q6["naziv"] + '"> <p class="tekst_uz_sliku">' + q6["tekst"] + '</p></div><footer><a href="../" class="nazad">' + nazad + '</a></footer></div><audio id="player"> <source id="ogg" src="zvuk/' + questionBank[questionNumber][7] + '.ogg" type="audio/ogg"> <source id="mp3" src="zvuk/' + questionBank[questionNumber][7] + '.mp3" type="audio/mp3"> </audio>');
            $("footer").fadeIn(300)

            $(".dalje").click(function() {
                    changeQuestion();

                })
                //puštanje zvuka
            var getaudio = $('#player')[0];
            /* Global variable for a timer. When the mouse is hovered over the speaker it will start playing after hovering for 1 second, if less than 1 second it won't play (incase you accidentally hover over the speaker) */
            var audiostatus = 'on';
            /* Global variable for the audio's status (off or on). It's a bit crude but it works for determining the status. */

            var zbuk = 0;
            $(document).on('click tap', '.speaker', function() {

                /* Touchend is necessary for mobile devices, click alone won't work */
                if (zbuk == 0) {
                    if (audiostatus == 'off') {
                        $(".speaker").css("background-position", "0 0");
                        getaudio.load();
                        getaudio.play();
                        zbuk = 1;
                        audiostatus = 'on';
                        return false;
                    } else if (audiostatus == 'on') {
                        $(".speaker").css("background-position", "0 0");
                        $('#player')[0].play()
                        moze = 1;
                        zbuk = 1;
                    }
                } else if (zbuk == 1) {
                    $('#player')[0].pause();
                    $(".speaker").css("background-position", "100% 0");

                    audiostatus = 'on';
                    zbuk = 0;
                }
            });

            $('#player').on('ended', function() {
                $(".speaker").css("background-position", "100% 0");
                /*When the audio has finished playing, remove the class speakerplay*/
                audiostatus = 'off';
                /*Set the status back to off*/
            });

        } else {
            if (rnd == 1) {
                q1 = questionBank[questionNumber][1];
                q2 = questionBank[questionNumber][2];
                q3 = questionBank[questionNumber][3];
                q4 = questionBank[questionNumber][4];
                q5 = questionBank[questionNumber][5];
                q6 = questionBank[questionNumber][6];
                q7 = questionBank[questionNumber][7];
                q8 = questionBank[questionNumber][8];
                q9 = questionBank[questionNumber][9];

            }
            if (rnd == 2) {
                q2 = questionBank[questionNumber][1];
                q3 = questionBank[questionNumber][2];
                q1 = questionBank[questionNumber][3];
                q4 = questionBank[questionNumber][4];
                q6 = questionBank[questionNumber][5];
                q5 = questionBank[questionNumber][6];
                q8 = questionBank[questionNumber][7];
                q7 = questionBank[questionNumber][8];
                q9 = questionBank[questionNumber][9];

            }
            if (rnd == 3) {
                q3 = questionBank[questionNumber][1];
                q1 = questionBank[questionNumber][2];
                q4 = questionBank[questionNumber][3];
                q2 = questionBank[questionNumber][4];
                q6 = questionBank[questionNumber][5];
                q5 = questionBank[questionNumber][6];
                q7 = questionBank[questionNumber][7];
                q8 = questionBank[questionNumber][8];
                q9 = questionBank[questionNumber][9];

            }

            if (rnd == 4) {
                q4 = questionBank[questionNumber][1];
                q1 = questionBank[questionNumber][2];
                q2 = questionBank[questionNumber][3];
                q3 = questionBank[questionNumber][4];
                q5 = questionBank[questionNumber][5];
                q6 = questionBank[questionNumber][6];
                q7 = questionBank[questionNumber][7];
                q8 = questionBank[questionNumber][8];
                q9 = questionBank[questionNumber][9];

            }
            if (rnd == 5) {
                q5 = questionBank[questionNumber][1];
                q1 = questionBank[questionNumber][2];
                q2 = questionBank[questionNumber][3];
                q3 = questionBank[questionNumber][4];
                q4 = questionBank[questionNumber][5];
                q6 = questionBank[questionNumber][6];
                q7 = questionBank[questionNumber][7];
                q8 = questionBank[questionNumber][8];
                q9 = questionBank[questionNumber][9];

            }

            if (rnd == 6) {
                q6 = questionBank[questionNumber][1];
                q1 = questionBank[questionNumber][2];
                q2 = questionBank[questionNumber][3];
                q3 = questionBank[questionNumber][4];
                q5 = questionBank[questionNumber][5];
                q4 = questionBank[questionNumber][6];
                q7 = questionBank[questionNumber][7];
                q8 = questionBank[questionNumber][8];
                q9 = questionBank[questionNumber][9];

            }

            if (rnd == 7) {
                q7 = questionBank[questionNumber][1];
                q1 = questionBank[questionNumber][2];
                q2 = questionBank[questionNumber][3];
                q3 = questionBank[questionNumber][4];
                q5 = questionBank[questionNumber][5];
                q4 = questionBank[questionNumber][6];
                q6 = questionBank[questionNumber][7];
                q8 = questionBank[questionNumber][8];
                q9 = questionBank[questionNumber][9];

            }

            if (rnd == 8) {
                q8 = questionBank[questionNumber][1];
                q1 = questionBank[questionNumber][2];
                q2 = questionBank[questionNumber][3];
                q3 = questionBank[questionNumber][4];
                q5 = questionBank[questionNumber][5];
                q4 = questionBank[questionNumber][6];
                q6 = questionBank[questionNumber][7];
                q7 = questionBank[questionNumber][8];
                q9 = questionBank[questionNumber][9];

            }

            if (rnd == 9) {
                q9 = questionBank[questionNumber][1];
                q1 = questionBank[questionNumber][2];
                q2 = questionBank[questionNumber][3];
                q3 = questionBank[questionNumber][4];
                q5 = questionBank[questionNumber][5];
                q4 = questionBank[questionNumber][6];
                q6 = questionBank[questionNumber][7];
                q7 = questionBank[questionNumber][8];
                q8 = questionBank[questionNumber][9];
            }

            $(stage).append('<div class="questionText"> <div class="speaker" id="fix"></div><a href="#" class="dalje" style="display:none;">dalje &raquo;</a> </div><div id="wrap"> <div id="1" class="pix"><img src="zvuk/' + q1["naziv"] + '"> <br><p class="tekst_uz_sliku">' + q1["tekst"] + '</p></div><div id="2" class="pix"><img src="zvuk/' + q2["naziv"] + '"> <p class="tekst_uz_sliku">' + q2["tekst"] + '</p></div><div id="3" class="pix"><img src="zvuk/' + q3["naziv"] + '"> <p class="tekst_uz_sliku">' + q3["tekst"] + '</p></div><div id="4" class="pix"><img src="zvuk/' + q4["naziv"] + '"> <p class="tekst_uz_sliku">' + q4["tekst"] + '</p></div><div id="5" class="pix"><img src="zvuk/' + q5["naziv"] + '"> <p class="tekst_uz_sliku">' + q5["tekst"] + '</p></div><div id="6" class="pix"><img src="zvuk/' + q6["naziv"] + '"> <p class="tekst_uz_sliku">' + q6["tekst"] + '</p></div><div id="7" class="pix"><img src="zvuk/' + q7["naziv"] + '"> <p class="tekst_uz_sliku">' + q7["tekst"] + '</p></div><div id="8" class="pix"><img src="zvuk/' + q8["naziv"] + '"> <p class="tekst_uz_sliku">' + q8["tekst"] + '</p></div><div id="9" class="pix"><img src="zvuk/' + q9["naziv"] + '"> <p class="tekst_uz_sliku">' + q9["tekst"] + '</p></div><footer><a href="../" class="nazad">' + nazad + '</a></footer></div><audio id="player"> <source id="ogg" src="zvuk/' + questionBank[questionNumber][10] + '.ogg" type="audio/ogg"> <source id="mp3" src="zvuk/' + questionBank[questionNumber][10] + '.mp3" type="audio/mp3"> </audio>');
            $(".dalje").click(function() {
                changeQuestion();

            })
            $("footer").fadeIn(300)

            //puštanje zvuka
            var getaudio = $('#player')[0];
            /* Global variable for a timer. When the mouse is hovered over the speaker it will start playing after hovering for 1 second, if less than 1 second it won't play (incase you accidentally hover over the speaker) */
            var audiostatus = 'on';
            /* Global variable for the audio's status (off or on). It's a bit crude but it works for determining the status. */

            var zbuk = 0;
            $(document).on('click tap', '.speaker', function() {

                /* Touchend is necessary for mobile devices, click alone won't work */
                if (zbuk == 0) {
                    if (audiostatus == 'off') {
                        $(".speaker").css("background-position", "0 0");
                        getaudio.load();
                        getaudio.play();
                        zbuk = 1;
                        audiostatus = 'on';
                        return false;
                    } else if (audiostatus == 'on') {
                        $(".speaker").css("background-position", "0 0");
                        $('#player')[0].play()
                        moze = 1;
                        zbuk = 1;
                    }
                } else if (zbuk == 1) {
                    $('#player')[0].pause();
                    $(".speaker").css("background-position", "100% 0");

                    audiostatus = 'on';
                    zbuk = 0;
                }
            });

            $('#player').on('ended', function() {
                $(".speaker").css("background-position", "100% 0");
                /*When the audio has finished playing, remove the class speakerplay*/
                audiostatus = 'off';
                /*Set the status back to off*/
            });

        }

        $('.pix').click(function() {
            if (moze == 1) {
                $('.pix').addClass("nohover");
                if (questionLock == false) {
                    questionLock = true;
                    //correct answer
                    if (this.id == rnd) {
                        score++;
                        $(stage).append('<div class="feedback1">' + tocno + '</div>');
                        setTimeout(function() {
                            $(".feedback1").hide();
                            $('#player')[0].pause();

                            //$(stage).append('<div class="feedback1">TOČNO</div>');
                            swal({
                                title: "" + questionBank[questionNumber][questionBank[questionNumber].length - 2],
                                html: '<img src="zvuk/' + questionBank[questionNumber][1]["naziv"] + ' " style="height:200px;"/><p style="text-align:justify">' + questionBank[questionNumber][questionBank[questionNumber].length - 1] + '</p>',
                                showCloseButton: true,
                                confirmButtonText: gumb,
                                confirmButtonColor: '#4FC5C8',
                                allowOutsideClick: false,
                                allowEscapeKey: false,
                                showCloseButton: false
                            }).then(function() {
                                setTimeout(function() {
                                    changeQuestion()
                                }, 1000);
                            })
                        }, 500);
                    }
                    //wrong answer	
                    if (this.id != rnd) {
                        $(stage).append('<div class="feedback2">' + pogresno + '</div>');
                        setTimeout(function() {
                            $(".feedback2").hide();
                            $('#player')[0].pause();

                            //$(stage).append('<div class="feedback1">TOČNO</div>');
                            swal({
                                title: toc_odg + questionBank[questionNumber][questionBank[questionNumber].length - 2],
                                html: '<img src="zvuk/' + questionBank[questionNumber][1]["naziv"] + ' " style="height:200px;"/><p style="text-align:justify">' + questionBank[questionNumber][questionBank[questionNumber].length - 1] + '</p>',
                                showCloseButton: true,
                                confirmButtonText: gumb,
                                allowOutsideClick: false,
                                confirmButtonColor: '#4FC5C8',
                                allowEscapeKey: false,
                                showCloseButton: false
                            }).then(function() {
                                setTimeout(function() {
                                    changeQuestion()
                                }, 1000);
                            })
                        }, 500);
                        $("#" + rnd).addClass("pulsiranje");


                    }

                }
            } else {
                $(stage).append('<div class="feedback2">' + upozorenje + '</div>');

                setTimeout(function() {
                    $(".feedback2").hide();
                }, 1500);
            }
        })
    } //display question






    function changeQuestion() {
        if (parseInt(window.innerWidth) <= 1080) {
            sirina = 1000
        } else {
            sirina = 1200
        }

        moze = 0;
        questionNumber++;
        $('#broj').html(questionNumber + 1 + "/" + 13)
        if (stage == "#game1") {
            stage2 = "#game1";
            stage = "#game2";
        } else {
            stage2 = "#game2";
            stage = "#game1";
        }
        if (questionNumber == 12) {
            gumb = kraj

        }
        if (questionNumber < numberOfQuestions) {
            displayQuestion();
        } else {
            displayFinalSlide();
        }

        $(stage2).animate({
            "right": "+=" + sirina + "px"
        }, "slow", function() {
            $(stage2).css('right', '-' + sirina + "px");
            $(stage2).empty();
        });

        $(stage).animate({
            "right": "+=" + sirina + "px"
        }, "slow", function() {
            questionLock = false;
        });
    } //change question




    function displayFinalSlide() {
        $("#uputa, #broj").hide();

        postotak = Math.round(score / numberOfQuestions * 100)

        if (postotak == 0) {

            ikona = "ikone/lubanja.png"
        } else if (postotak < 50)

        {
        ikona = "ikone/los.png"
        } else if (postotak >= 50 || postotak <= 70) {
            ikona = "ikone/osrednji.png"
        } else if (postotak >= 71 || postotak <= 80) {
            ikona = "ikone/solidan.png"
        } else if (postotak >= 81 || postotak <= 90) {
            ikona = "ikone/vrlo_dobar.png"
        } else if (postotak >= 91) {
            ikona = "ikone/izvrstan.png"
        }


        if (localStorage.getItem("jezik") == "hr") {
            $(stage).append('<div class="questionText">Završili ste kviz!<br><br>Broj pitanja: ' + numberOfQuestions + '<br>Broj točnih odgovora: ' + score + '<br>Postotak: ' + postotak + '% <br><br><img src="' + ikona + '" class="ikona"/><br><br><a href="#" onclick="location.reload()">nova igra</a></div>')
        } else {
            $(stage).append('<div class="questionText">You have finished the game!<br><br>Number of questions: ' + numberOfQuestions + '<br>Number of correct answers: ' + score + '<br>Percentage: ' + postotak + '% <br><br><img src="' + ikona + '" class="ikona"/><br><br><a href="#" onclick="location.reload()">new game</a></div>')
        }

        $(".questionText").css({
            "position": "relative",
            "top": "50%",
            "-ms-transform": "translateY(50%)",
            "-webkit-transform": "translateY(50%)",
            "transform": "translateY(50%)"
        })

    } //display final slide

}); //doc ready

// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 

modal.style.display = "block";

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}