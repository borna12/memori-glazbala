<?php return array (
  'application' => 
  array (
    'debug' => false,
  ),
  'database' => 
  array (
    'default' => 'sqlite',
    'connections' => 
    array (
      'sqlite' => 
      array (
        'prefix' => 'pk_',
      ),
    ),
  ),
  'system' => 
  array (
    'secret' => '.7x19pDLPxxnn79lXZyvUKD0qHz2bP4Ekj8FeLKG0jv16tgFC32r7y7yuuRal75y',
  ),
);