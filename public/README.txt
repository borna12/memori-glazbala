A Pen created at CodePen.io. You can find this one at https://codepen.io/anatravas/pen/qqbBeJ.

 Animated 3D card flip done in CSS and a little jQuery to help handle clicks and add and remove CSS classes.  CSS animations powered by Animista.net